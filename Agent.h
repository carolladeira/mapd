//
// Created by carol on 12/17/18.
//

#ifndef MAPD_AGENT_H
#define MAPD_AGENT_H

#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <functional>  // for std::hash (c++11 and above)
#include <map>
#include <algorithm>
#include <queue>

//#include "Node.h"
#include "Endpoint.h"
class Task;
class Token;


using namespace std;


class Agent {
public:
    int loc;
    int id;
    int row, col;

    int finish_time;
    vector<int> path;

  //  vector<Node*>open;

    void setAgent(int loc, int col, int row, int id);
    bool TP( Token &token);
    Task* bestTask(Token token);

    int AStar(int start_loc, Endpoint *goal, int begin_time, Token token, bool coleta);
    bool isConstrained(int curr_id, int next_id, int next_timestep, Token token, int ag_hide);
    void updatePath(Node *goal);
    void sort_openList();
    void releaseClosedListNodes(map<unsigned int, Node*> &allNodes_table);
    bool Move2EP(Token &token);

    virtual ~Agent();

};
class Task{
public:

    int id;
    int state; //0: livre | 1:com agente;
    Agent *agent;

    int timestep_task_arrive;//tempo que a tarefa chegou no sistema;
    int start_time;
    int goal_time;

    int ag_arrive_start;
    int ag_arrive_goal;

    Endpoint *start;
    Endpoint *goal;

    Task();
    Task(int id, int t, Endpoint *s, Endpoint *g, int s_t, int g_t):id(id), timestep_task_arrive(t), start(s), goal(g), start_time(s_t), goal_time(g_t) {}

    virtual ~Task() {

    }
};

class Token{
public:
    int timestep;

    vector<bool> map;
    vector<bool> endpoints;
    vector<Agent*> agents;

    vector<vector<int>> path; //path[agent][time] = loc (id)

    list<Task*> taskset;

    Token(){timestep = 0;};

    virtual ~Token() {

    }


};

#endif //MAPD_AGENT_H
