//
// Created by carol on 12/17/18.
//

#include "Agent.h"

#define maxtime 10000

bool meujeito(const Node *i, const Node *j) { return i->g_val + i->h_val > j->g_val + j->h_val; }


void Agent::setAgent(int loc, int col, int row, int id) {
    this->loc = loc;
    this->col = col;
    this->row = row;
    this->id = id;
    this->finish_time = 0;
    for (int i = 0; i < maxtime; i++) {
        path.push_back(loc);//stay still all the tiem
    }

}

void Agent::updatePath(Node *goal) //update path for agent
{
    //hold the goal
    for (int i = goal->timestep + 1; i < path.size(); i++) {
        this->path[i] = goal->loc;
    }
    //update the path
    Node *curr = goal;
    while (curr != NULL) {
        this->path[curr->timestep] = curr->loc;
        //     cout <<id<< "TEMPO " << curr->timestep << " - " <<path[curr->timestep] <<endl;
        curr = curr->parent;
    }

}

//agente esta com o token
bool Agent::TP(Token &token) {

    loc = path[token.timestep];

    Task *task = NULL;
    //ordena task por h value;
    task = bestTask(token);
    if (task == NULL) {

        ///se agente esta na posicao de entrega de alguma tarefa do taskset, move para um endpoint
        bool move = false;
        for (list<Task *>::iterator it = token.taskset.begin(); it != token.taskset.end(); it++) {
            if ((*it)->goal->loc == loc) //move away
            {
                move = true;
                break;
            }
        }
        //move para um endpoint
        if (move) {
        //  cout << "Agent "<<id<<"move para o endpoint mais perto" << endl;
            if (Move2EP(token)) {
                for (int i = token.timestep; i < token.path[id].size(); i++) //agent move with package or waiting
                {
                    token.path[id][i] = path[i];
                }
                return true;
            }

        }
            //o agente continua na posicao que esta por mais um timestep
        else {
          //  std::cout << "Agent " << id << " wait at timestep " << token.timestep << endl;
            this->finish_time = token.timestep + 1;
            this->path[token.timestep + 1] = loc;
            return true;
        }
        //tem tarefa a ser pega
    } else {

        //posicao do robo ate posicao de coleta
        int arrive_start = AStar(loc, task->start, token.timestep, token, true);
        //posicao de coleta ate posicao de entrega
        int arrive_goal = AStar(task->start->loc, task->goal, arrive_start, token, false);

        if (arrive_goal == -1 || arrive_start == -1) {
            cout << "ERRO: nao encontrou caminho" << endl;
            system("PAUSE");

        }
        for (int i = token.timestep; i < token.path[id].size(); i++) //agent move with package or waiting
        {
            token.path[id][i] = path[i];
        }
        this->finish_time = arrive_goal;
   //     std::cout << "Agent " << id << " take task " << task->start->loc << " --> " << task->goal->loc;
   //     std::cout << "	Timestep " << token.timestep << "-->" << arrive_goal << endl;

        //update task
        task->agent = this;
        task->ag_arrive_start = arrive_start;
        task->ag_arrive_goal = arrive_goal;

        //    cout << "Task " << task->start->loc << "-->" << task->goal->loc << " is done at Timestep " << task->ag_arrive_goal << endl;
        token.taskset.remove(task);
        //delete(task);
        return true;

    }
    return false;

}

//g(n) = custo de atual ate n
//h(n) = custo de n ate goal
//f(n) = g(n) + h(n)
int Agent::AStar(int start_loc, Endpoint *goal, int begin_time, Token token, bool coleta) {

    // open.clear();
    vector<Node *> open;
    int goal_location = goal->loc;
    map<unsigned int, Node *> allNodes_table; //key = g_val*map_size+loc

    Node *start = new Node(start_loc, 0, goal->h_val[start_loc], NULL, begin_time, false);

    open.push_back(start);
    start->in_openList = true;
    allNodes_table.insert(make_pair(start_loc, start)); //g_val=0 -->key=loc

    while (!open.empty()) {
        // sort_openList();
        sort(open.begin(), open.end(), meujeito);
        Node *curr = open.back();
        open.pop_back();
        curr->in_openList = false;

        if (curr->loc == goal_location) {
            bool hold = true;

//            if (coleta) {
//                for (int j = 0; j < token.agents.size(); j++) {
//                    if (j != id && curr->loc == token.path[j][curr->timestep + 1]) {
//                        //fazer uma funcao q se ele for de coleta talvez ele possa ir praa aquele lugar
//                        cout << "Agente " << j << " com " << id << " tempo " << curr->timestep + 1 << " no lugar "
//                             << curr->loc << endl;
//                        hold = false;
//                        break;
//                    }
//                }
//            }
//            else {
            for (int i = curr->timestep + 1; i < maxtime; i++) {
                for (int j = 0; j < token.agents.size(); j++) {
                    if (j != id && curr->loc == token.path[j][i]) {
                        //fazer uma funcao q se ele for de coleta talvez ele possa ir praa aquele lugar
                        //  cout << "Agente " << j << " com " << id << " tempo " << i << " no lugar " << curr->loc
                      //   << endl;
                        hold = false;
                        break;
                    }
                }
            }
           //  }
            if (hold)
            {
                updatePath(curr);
                int t = curr->timestep;
                releaseClosedListNodes(allNodes_table);
                return t;
            }
        }
        int next_loc;

        int action[5] = {0, 1, -1, col, -col};
        for (int i = 0; i < 5; i++) {
            next_loc = curr->loc + action[i];
            int next_timestep = curr->timestep + 1;
         //   cout<<endl<<curr->loc<<" -> "<<next_loc<<" no tempo "<<next_timestep;
            if (!isConstrained(curr->loc, next_loc, next_timestep, token, 0)) {
              //\  cout<<" Sem restricoes";
                int next_g_val = curr->g_val + 1;
                int next_h_val = goal->h_val[next_loc];

                //generate (maybe temporary) node
                Node *next = new Node(next_loc, next_g_val, next_h_val, curr, next_timestep, false);

                //try to retrieve it from the hash table
                map<unsigned int, Node *>::iterator it = allNodes_table.find(next->loc + next->g_val * row * col);
                if (it == allNodes_table.end()) //undiscover
                {  // add the newly generated node to open_list and hash table
                    next->in_openList = true;

                    allNodes_table.insert(pair<unsigned int, Node *>(next->loc + next->g_val * row * col, next));
                    open.push_back(next);
                } else //discovered
                {
                    delete (next);
                }
            }
        }
    }
    releaseClosedListNodes(allNodes_table);
    return -1;

}

//escolhe a tarefa com menor h-value
//e que nenhum caminho de outro agente termine no posicao de coleta ou entrega da tarefa
Task *Agent::bestTask(Token token) {
    vector<bool> hold(col * row, false);
    for (int i = 0; i < token.path.size(); i++) {
        if (i != id) {
            int loc_f = token.path[i][token.path[i].size() - 1];
            hold[loc_f] = true;
        }
    }

    Task *task = NULL;
    list<Task *>::iterator n;
    for (list<Task *>::iterator it = token.taskset.begin(); it != token.taskset.end(); it++) {
        bool p = hold[(*it)->start->loc];
        if (hold[(*it)->start->loc] == true || hold[(*it)->goal->loc] == true) continue;
        if (task == NULL) task = *it;
        unsigned int t = task->start->h_val[loc];
        unsigned int m = (*it)->start->h_val[loc];
        if ((*it)->start->h_val[loc] < task->start->h_val[loc]) {
            task = *it;
        }
    }
    return task;
}

bool Agent::isConstrained(int curr_loc, int next_loc, int next_timestep, Token token, int ag_hide) {

    if (!token.map[next_loc]) return true;

    for (int i = 0; i < token.path.size(); i++) {
        if (i == id) continue;
        else if (token.path[i][next_timestep] == next_loc) return true;
        else if (token.path[i][next_timestep] == curr_loc && token.path[i][next_timestep - 1] == next_loc) return true;
    }

    return false;
}


void Agent::releaseClosedListNodes(map<unsigned int, Node *> &allNodes_table) {
    map<unsigned int, Node *>::iterator it;
    for (it = allNodes_table.begin(); it != allNodes_table.end(); it++) {
        delete ((*it).second);
    }
    allNodes_table.clear();

}

bool Agent::Move2EP(Token &token) {
    //BFS algorithm, choose the first empty endpoint to go to
    queue<Node *> Q;
    map<unsigned int, Node *> allNodes_table; //key = g_val * map_size + loc
    int action[5] = {0, 1, -1, col, -col};
    Node *start = new Node(loc, 0, NULL, token.timestep);
    allNodes_table.insert(make_pair(loc, start)); //g_val = 0 --> key = loc
    Q.push(start);
    while (!Q.empty()) {
        Node *v = Q.front();
        Q.pop();
        if (v->timestep >= maxtime - 1) continue; // time limit
        if (token.endpoints[v->loc]) // if v->loc is an endpoint
        {
            bool occupied = false;
            // check whether v->loc can be held (no collision with other agents)
            for (unsigned int t = v->timestep; t < maxtime && !occupied; t++) {
                for (unsigned int ag = 0; ag < token.agents.size() && !occupied; ag++) {
                    if (ag != id && token.path[ag][t] == v->loc) occupied = true;
                }
            }
            // check whether it is a goal of a task
            for (list<Task *>::iterator it = token.taskset.begin(); it != token.taskset.end() && !occupied; it++) {
                if ((*it)->goal->loc == v->loc) occupied = true;
            }
            if (!occupied)// If this endpoint is empty, return path
            {
                updatePath(v);
                finish_time = v->timestep;
                //cout << "Agent " << id << " moves to endpoint " << v->loc << endl;
                releaseClosedListNodes(allNodes_table);
                return true;
            }
            // Else, keep searching
        }
        for (int i = 0; i < 5; i++) // search its neighbor
        {
            if (!isConstrained(v->loc, v->loc + action[i], v->timestep + 1, token, id)) {
                //try to retrieve it from the hash table
                map<unsigned int, Node *>::iterator it = allNodes_table.find(
                        v->loc + action[i] + (v->g_val + 1) * row * col);
                if (it == allNodes_table.end()) //undiscover
                {  // add the newly generated node to hash table
                    Node *u = new Node(v->loc + action[i], v->g_val + 1, v, v->timestep + 1);
                    allNodes_table.insert(pair<unsigned int, Node *>(u->loc + u->g_val * row * col, u));
                    Q.push(u);
                }
            }
        }
    }
    return false;
}

Agent::~Agent() {

}
