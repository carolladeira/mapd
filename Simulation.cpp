//
// Created by carol on 12/17/18.
//

#include "Simulation.h"

#define maxtime1 10000

Simulation::Simulation(string map, string task, float frequency) {

    LoadMap(map);
    LoadTask(task, frequency);
}

void Simulation::LoadMap(string arq) {


    string line;
    ifstream myfile(arq);
    cout<<arq<<endl;
    if (!myfile.is_open())
    {
        cout << "Map file not found." << endl;
        system("PAUSE");
        return;
    }
    //read file

    cout << "Lendo arquivo" << endl;

    getline(myfile, line);
    sscanf(line.c_str(), "%d" "%d", &row, &col);

    row = row+2;
    col = col+2;
    stringstream ss;
    getline(myfile, line);
    ss << line;
    ss >> num_endpoint; //number of endpoints that may have tasks on. Other endpoints are home endpoints


    ss.clear();
    getline(myfile, line);
    ss << line;
    ss >> num_agents; //agent number

    ss.clear();
    getline(myfile, line);
    ss << line;
    ss >> maxtime; //max timestep


    agents.resize(num_agents);
    token.map.resize(row*col);
    token.agents.resize(num_agents);
    token.path.resize(num_agents);
    token.endpoints.resize(row*col);
    endpoints.resize(num_agents+num_endpoint); //endpoints.resize(workpoint_num + agentes);

    int ep=0, ag=0;
    for(int i = 1; i < row-1 ; i++){
        getline(myfile, line);

        for(int j = 1; j < col-1; j++){

            if(line[j - 1]=='@') continue;
            token.map[col*i +j] = true;
            token.endpoints[col*i + j] = (line[j - 1] == 'e') || (line[j - 1] == 'r'); //endpoint
            if(line[j -1]=='e')
            {
                endpoints[ep].loc = i*col+j;
                endpoints[ep].col = j;
                endpoints[ep].row = i;
                ep++;
              //  cout << "E[" << j << "," << i << "] "<<i*col+j;
            }else if(line[j - 1] == 'r'){ // posicao inicial do robo, endpoint de robo.

                agents[ag].setAgent(i*col + j, col, row, ag);
                token.agents[ag] = &agents[ag];
                token.path[ag].resize(maxtime1);
                endpoints[num_endpoint + ag].loc = i*col + j;
                endpoints[num_endpoint + ag].col = col;
                endpoints[num_endpoint + ag].row = row;
                for(int p=0; p < maxtime1; p++){
                    token.path[ag][p] = i*col +j;
                }
               // token.agents[ag].
                ag++;
            }
        }
    }

    myfile.close();

    //coloca falso em volta do mapa, pra nao ser alcancavel
    for(int i=0; i <row; i++){
        token.map[i*col]=false;
        token.map[i*col + col - 1] = false;
        token.endpoints[i*col] = false;
        token.endpoints[i*col + col -1] = false;
    }

    for(int i=0; i <col - 1; i++){
        token.map[i*col]=false;
        token.map[i*col + col - 1] = false;
        token.endpoints[i*col] = false;
        token.endpoints[i*col + col -1] = false;
    }

    //inicializa matriz com todos os h-values
    for(int e=0; e < endpoints.size(); e++){
        endpoints[e].setH_val(token.map, col);
        endpoints[e].id = e;
    }
  //  printMap();
}

void Simulation::LoadTask(string arq, float frequency) {

    string line;
    ifstream myfile(arq);
    cout<<arq<<endl;
    if (!myfile.is_open())
    {
        cout << "Task file not found." << endl;
        system("PAUSE");
        return;
    }
    int tam;

    stringstream ss;
    getline(myfile, line);
    ss << line;
    ss >> num_task;

    ss.clear();
    getline(myfile, line);
    ss << line;
    ss >> tam;



    int num_timesteps = 34;
   // num_timesteps = num_task/frequency;
    taskset.resize(tam);//arrumar isso, na hora do arquivo, colocar a quantidade de timesteps, nao quantidade tarefas ESQUECE ISSO

    for(int i=0; i< num_task; i++){

        int t,s,g,ts,tg;
        ss.clear();
        getline(myfile, line);
        ss << line;
        ss >> t >> s >> g >> ts >> tg;
        Endpoint *x = &endpoints[s];
        Task novo = Task(i, t, &endpoints[s], &endpoints[g], ts, tg);
      // cout<<"TASK START: "<<novo.start->id<<" loc:"<<novo.start->loc<<" row:"<<novo.start->row<<" col:"<<novo.start->col<< endl;
      //  cout<<"TASK GOAL: "<<novo.goal->id<<" loc:"<<novo.goal->loc<<" row:"<<novo.goal->row<<" col:"<<novo.goal->col<< endl;
      //  cout<<t<<"\t"<<novo.start->loc<<"\t"<<novo.goal->loc<< endl;
        t_task = t;
        taskset[t].push_back(novo);


    }
    myfile.close();

    if (!taskset[0].empty())
    {
        for (vector<Task>::iterator it = taskset[0].begin(); it != taskset[0].end(); it++)
        {
            token.taskset.push_back(&(*it));
        }
    }

}

void Simulation::run_TP() {


    Agent *ag;
    int t_f =0;
    ag = &agents[0];
    while(!token.taskset.empty() ||token.timestep <= t_task){
        ag = &agents[0];
        for(int i=0; i < agents.size(); i ++){
            if(agents[i].finish_time == token.timestep){
                ag = &agents[i];
                break;
            }else if(agents[i].finish_time < ag->finish_time){
                ag= &agents[i];
            }
        }
        for(int i= token.timestep + 1; i <=ag->finish_time; i++){
            if(i >= taskset.size()){
                // cout<<"i 'e maior q tamanho do taskset"<<endl;
                break;
            }
            if(taskset[i].empty()) continue;
            for (vector<Task>::iterator it = taskset[i].begin(); it != taskset[i].end(); it++)
            {
                token.taskset.push_back(&(*it));
            }
        }


        token.timestep = ag->finish_time;
        ag->loc = ag->path[token.timestep];

        if (token.taskset.empty())//If no new tasks
        {
            ag->finish_time = ag->finish_time + 1; //agent waits for one timestep
            continue;
        }

        time++;

        clock_t start = clock();
        if(!ag->TP(token)){
            cout<<"Nao pegou tarefas"<< endl;
            system("PAUSE");

        }
        final += start;
        computation_time += clock() - start;
        if (!testCollision())
        {
            system("PAUSE");
            return;
         }


    }

}

void Simulation::SavePath() {

}
void Simulation::SaveTask() {

}

void Simulation::printMap(){

    ofstream fout("/home/carol/Desktop/MAPD/mapa-impresso.txt", ios::app);
    if (!fout) return;

    for(int i=0; i < row; i ++){
        for(int j=0; j < col; j++){
            fout << i << "," << j << "|" << col*i +j<<"|-";



        }
        fout<<endl;
    }
    int i=0;
    fout<<"TOKEN ENDPOINTS:"<<endl;
    while(token.endpoints.size()> i){
        if(token.endpoints[i] == true) fout<<i<<"E - ";
        i++;
    }
    i=0;
    fout<<endl;
    fout<<"AGENTES:"<<endl;
    while(agents.size()> i){
        fout<<i<<" " << agents[i].loc <<" - ";
        i++;
    }

    ///   fout << instance_name << " " << LastFinish << " " << WaitingTime << " " << computation_time / (double)LastFinish << endl;


    fout.close();


}

void Simulation::showTask(string arq, float j, int c) {

    unsigned int WaitingTime = 0;
    unsigned int LastFinish = 0;
    cout << endl << "TASK" << endl;
    for (unsigned int i = 0; i < taskset.size(); i++) {
        if (taskset[i].size() > 0) {
            cout << "\tTimestep " << i << " :	"<<endl;
            for (vector<Task>::iterator it = taskset[i].begin(); it != taskset[i].end(); it++) {
                cout << "Agent " << it->agent->id << " delivers package from " << it->id << " - " << it->start->loc
                     << " to " << it->goal->loc
                     << "\t(" << it->ag_arrive_start << "," << it->ag_arrive_goal << ")" << endl;
                WaitingTime += it->ag_arrive_goal - it->timestep_task_arrive;
                LastFinish = LastFinish > it->ag_arrive_goal ? LastFinish : it->ag_arrive_goal;
            }
            // cout << "	";
        }
    }
    cout << endl << "Finishing Timestep:	" << LastFinish << endl;
    cout << "Sum of Task Waiting Time:	" << WaitingTime << endl;
    cout << "Computation time:	" << computation_time/ (double)LastFinish << endl;

    ofstream fout(arq, ios::app);
    if (!fout) return;
   // fout << "sep=," <<endl;
  //  fout << "Makespan" << "," << "Service time" << "," << "Run time"<<endl;
    fout <<c<<","<< LastFinish << "," <<WaitingTime<<","<<computation_time/ (double)LastFinish<<","<<WaitingTime / num_task << "," <<j<<endl;
    fout.close();


}

bool Simulation::testCollision(){

    for (unsigned int ag = 0; ag < agents.size(); ag++)
    {
        for (unsigned int i = ag + 1; i < agents.size(); i++)
        {
            //j=0;
            for (int j=token.timestep+1; j < maxtime1; j++)
            {
                if (agents[ag].path[j] == agents[i].path[j])
                {
                    cout << "Agent " << ag << " and " << i << " collide at location "
                         << agents[ag].path[j] << " at time " << j << endl;

                    cout<<ag<<" "<<i<<endl;
                    for(int t=0;t<j;t++){
                        cout<<t<<token.path[ag][t]<<" "<<agents[i].path[t]<<endl;
                    }
                    return false;
                }
                else if (token.timestep > 0 && agents[ag].path[j] == agents[i].path[j - 1]
                         && agents[ag].path[j - 1] == agents[i].path[j])
                {
                    cout << "Agent " << ag << " and " << i << " collide at edge "
                         << agents[ag].path[j - 1] << "-" << agents[ag].path[j] << " at time " << j << endl;
                    cout<<ag<<" "<<i<<endl;
                    for(int t=0;t<j;t++){
                        cout<<t<<token.path[ag][t]<<" "<<agents[i].path[t]<<endl;
                    }
                    return false;
                }

            }
        }
    }
    return true;
}

void Simulation::showPathAgents() {

    for(int i=0; i < token.path.size(); i++){
        cout << "TOKEN PATH: AG " << i << " - Tempo final:" <<agents[i].finish_time <<endl;
        for(int j=0; j <=agents[i].finish_time; j ++){
          //  cout << j <<" ";
            cout << token.path[i][j] <<" ";
        }
        cout<<endl;
    }

}

Simulation::~Simulation() {

}
