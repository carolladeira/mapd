//
// Created by carol on 12/17/18.
//

#ifndef MAPD_SIMULATION_H
#define MAPD_SIMULATION_H

#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <cassert>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <climits>

#include "Agent.h"
#include "Endpoint.h"

using namespace std;

class Simulation {
public:
    int time, t_task;
    float computation_time;
    float final;
    int num_task;

    int row, col;
    int maxtime;
    Token token;
    int num_agents;
    int num_endpoint;

    vector<Agent> agents;
    vector<Endpoint> endpoints;

    vector<vector<Task>> taskset;

    ~Simulation();

    Simulation () {};
    Simulation(string map, string task, float frequency);

    void LoadMap(string arq);
    void LoadTask(string arq, float frequency);

    void run_TP();

    void showTask(string arq, float j, int c);
    void showPathAgents();

    ///save
    void SavePath();
    void SaveTask();

    ///Para eu conferir
    void printMap();
    bool testCollision();


};


#endif //MAPD_SIMULATION_H
